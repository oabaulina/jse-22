
package ru.baulina.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.baulina.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AbstractEntity_QNAME = new QName("http://endpoint.tm.baulina.ru/", "abstractEntity");
    private final static QName _ChangeUserPassword_QNAME = new QName("http://endpoint.tm.baulina.ru/", "changeUserPassword");
    private final static QName _ChangeUserPasswordResponse_QNAME = new QName("http://endpoint.tm.baulina.ru/", "changeUserPasswordResponse");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.baulina.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.baulina.ru/", "createUserResponse");
    private final static QName _CreateUserWithEmail_QNAME = new QName("http://endpoint.tm.baulina.ru/", "createUserWithEmail");
    private final static QName _CreateUserWithEmailResponse_QNAME = new QName("http://endpoint.tm.baulina.ru/", "createUserWithEmailResponse");
    private final static QName _CreateUserWithRole_QNAME = new QName("http://endpoint.tm.baulina.ru/", "createUserWithRole");
    private final static QName _CreateUserWithRoleResponse_QNAME = new QName("http://endpoint.tm.baulina.ru/", "createUserWithRoleResponse");
    private final static QName _ProfileOfUserChange_QNAME = new QName("http://endpoint.tm.baulina.ru/", "profileOfUserChange");
    private final static QName _ProfileOfUserChangeResponse_QNAME = new QName("http://endpoint.tm.baulina.ru/", "profileOfUserChangeResponse");
    private final static QName _Session_QNAME = new QName("http://endpoint.tm.baulina.ru/", "session");
    private final static QName _User_QNAME = new QName("http://endpoint.tm.baulina.ru/", "user");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.baulina.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChangeUserPassword }
     * 
     */
    public ChangeUserPassword createChangeUserPassword() {
        return new ChangeUserPassword();
    }

    /**
     * Create an instance of {@link ChangeUserPasswordResponse }
     * 
     */
    public ChangeUserPasswordResponse createChangeUserPasswordResponse() {
        return new ChangeUserPasswordResponse();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link CreateUserWithEmail }
     * 
     */
    public CreateUserWithEmail createCreateUserWithEmail() {
        return new CreateUserWithEmail();
    }

    /**
     * Create an instance of {@link CreateUserWithEmailResponse }
     * 
     */
    public CreateUserWithEmailResponse createCreateUserWithEmailResponse() {
        return new CreateUserWithEmailResponse();
    }

    /**
     * Create an instance of {@link CreateUserWithRole }
     * 
     */
    public CreateUserWithRole createCreateUserWithRole() {
        return new CreateUserWithRole();
    }

    /**
     * Create an instance of {@link CreateUserWithRoleResponse }
     * 
     */
    public CreateUserWithRoleResponse createCreateUserWithRoleResponse() {
        return new CreateUserWithRoleResponse();
    }

    /**
     * Create an instance of {@link ProfileOfUserChange }
     * 
     */
    public ProfileOfUserChange createProfileOfUserChange() {
        return new ProfileOfUserChange();
    }

    /**
     * Create an instance of {@link ProfileOfUserChangeResponse }
     * 
     */
    public ProfileOfUserChangeResponse createProfileOfUserChangeResponse() {
        return new ProfileOfUserChangeResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbstractEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "abstractEntity")
    public JAXBElement<AbstractEntity> createAbstractEntity(AbstractEntity value) {
        return new JAXBElement<AbstractEntity>(_AbstractEntity_QNAME, AbstractEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUserPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "changeUserPassword")
    public JAXBElement<ChangeUserPassword> createChangeUserPassword(ChangeUserPassword value) {
        return new JAXBElement<ChangeUserPassword>(_ChangeUserPassword_QNAME, ChangeUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeUserPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "changeUserPasswordResponse")
    public JAXBElement<ChangeUserPasswordResponse> createChangeUserPasswordResponse(ChangeUserPasswordResponse value) {
        return new JAXBElement<ChangeUserPasswordResponse>(_ChangeUserPasswordResponse_QNAME, ChangeUserPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "createUserWithEmail")
    public JAXBElement<CreateUserWithEmail> createCreateUserWithEmail(CreateUserWithEmail value) {
        return new JAXBElement<CreateUserWithEmail>(_CreateUserWithEmail_QNAME, CreateUserWithEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "createUserWithEmailResponse")
    public JAXBElement<CreateUserWithEmailResponse> createCreateUserWithEmailResponse(CreateUserWithEmailResponse value) {
        return new JAXBElement<CreateUserWithEmailResponse>(_CreateUserWithEmailResponse_QNAME, CreateUserWithEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "createUserWithRole")
    public JAXBElement<CreateUserWithRole> createCreateUserWithRole(CreateUserWithRole value) {
        return new JAXBElement<CreateUserWithRole>(_CreateUserWithRole_QNAME, CreateUserWithRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "createUserWithRoleResponse")
    public JAXBElement<CreateUserWithRoleResponse> createCreateUserWithRoleResponse(CreateUserWithRoleResponse value) {
        return new JAXBElement<CreateUserWithRoleResponse>(_CreateUserWithRoleResponse_QNAME, CreateUserWithRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileOfUserChange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "profileOfUserChange")
    public JAXBElement<ProfileOfUserChange> createProfileOfUserChange(ProfileOfUserChange value) {
        return new JAXBElement<ProfileOfUserChange>(_ProfileOfUserChange_QNAME, ProfileOfUserChange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileOfUserChangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "profileOfUserChangeResponse")
    public JAXBElement<ProfileOfUserChangeResponse> createProfileOfUserChangeResponse(ProfileOfUserChangeResponse value) {
        return new JAXBElement<ProfileOfUserChangeResponse>(_ProfileOfUserChangeResponse_QNAME, ProfileOfUserChangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Session }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "session")
    public JAXBElement<Session> createSession(Session value) {
        return new JAXBElement<Session>(_Session_QNAME, Session.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.baulina.ru/", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

}
