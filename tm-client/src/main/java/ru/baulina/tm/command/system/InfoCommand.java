package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.util.SystemInformation;

public final class InfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about system.";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + SystemInformation.availableProcessors);
        System.out.println("Free memory: " + SystemInformation.freeMemoryFormat);
        System.out.println("Maximum memory: " + SystemInformation.maxMemoryFormat);
        System.out.println("Total memory available to JVM: " + SystemInformation.totalMemoryFormat);
        System.out.println("Used memory by JVM: " + SystemInformation.usedMemoryFormat);
        System.out.println("OK");
        System.out.println();
    }

}
