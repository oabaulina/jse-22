package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.endpoint.User;

import java.util.List;

public final class UserShowCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "list-users";
    }

    @NotNull
    @Override
    public String description() {
        return "Show users.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = getSession();
        final List<User> users = endpointLocator.getAdminUserEndpoint().getUserList(session);
        System.out.println("[SHOW_LIST_USERS]");
        int index = 1;
        for (User user: users) {
            System.out.println(index + ". ");
            System.out.println("LOGIN: " + user.getLogin());
            System.out.println("E-MAIL: " + user.getEmail());
            System.out.println("FEST NAME: " + user.getFirstName());
            System.out.println("LAST NAME: " + user.getLastName());
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
