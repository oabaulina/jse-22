package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;

public final class ProjectClearCommand extends AbstractProjectCommand {


    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        @Nullable final Session session = getSession();
        endpointLocator.getProjectEndpoint().clearProjects(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
