package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;

public final class DataJsonClearCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete json data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON DELETE]");
        @Nullable final Session session = getSession();
        endpointLocator.getAdminDampEndpoint().dataJasonClear(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
