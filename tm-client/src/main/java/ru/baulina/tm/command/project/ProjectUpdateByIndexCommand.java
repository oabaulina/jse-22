package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Project;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-update_by_index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index  = TerminalUtil.nexInt() -1;
        @Nullable final Session session = getSession();
        @Nullable final Project project = endpointLocator.getProjectEndpoint().removeOneProjectByIndex(session, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        @Nullable final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description  = TerminalUtil.nextLine();
        @Nullable final Project projectUpdated = endpointLocator.getProjectEndpoint().updateProjectByIndex(
                session, index, name, description
        );
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
