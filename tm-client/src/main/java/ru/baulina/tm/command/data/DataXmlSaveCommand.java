package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;

public final class DataXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        @Nullable final Session session = getSession();
        endpointLocator.getAdminDampEndpoint().dataXmlSave(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
