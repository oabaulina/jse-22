package ru.baulina.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class Domain implements Serializable {

    @Nullable
    private List<Project> projects = new ArrayList<>();

    @Nullable
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    private List<User> users = new ArrayList<>();

}
