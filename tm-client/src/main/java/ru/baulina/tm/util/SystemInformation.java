package ru.baulina.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class SystemInformation {

    public static int availableProcessors = Runtime.getRuntime().availableProcessors();

    private static final long freeMemory = Runtime.getRuntime().freeMemory();

    @NotNull
    public static String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);

    private static final long maxMemory = Runtime.getRuntime().maxMemory();

    @NotNull
    private static final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);

    @NotNull
    public static String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;

    private static final long totalMemory = Runtime.getRuntime().totalMemory();

    @NotNull
    public static String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);

    private static final long usedMemory = totalMemory - freeMemory;

    @NotNull
    public static String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);

}
