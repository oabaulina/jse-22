package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.util.HashUtil;

import java.util.List;
import java.util.Objects;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User findById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User removeById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public void load(final List<User> users) {
        userRepository.load(users);
    }

    @Override
    public void lockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
    }

    @Override
    public void unlockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
    }

    @Override
    public void changePassword(
            @Nullable final String passwordOld,
            @Nullable final String passwordNew,
            @Nullable final Long userId
    ) {
        final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        final String hashOld = HashUtil.salt(passwordOld);
        if (hashOld == null) throw new AccessDeniedException();
        if (!hashOld.equals(user.getPasswordHash())) throw new AccessDeniedException();
        final String hashNew = HashUtil.salt(passwordNew);
        user.setPasswordHash(hashNew);
    }

    @Override
    public void changeUser(
            @Nullable final String email,
            @Nullable final String festName,
            @Nullable final String LastName,
            @Nullable final Long userId
    ) {
        final User user = findById(userId);
        user.setEmail(email);
        user.setFirstName(festName);
        user.setLastName(LastName);
    }

}
