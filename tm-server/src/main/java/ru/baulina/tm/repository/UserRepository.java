package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.api.repository.IUserRepository;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final List<User> users = getList();
        return users;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        @NotNull final List<User> users = getList();
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final Long id) {
        @NotNull final List<User> users = getList();
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeUser(@NotNull final User user) {
        @NotNull final List<User> users = getList();
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final List<User> users = getList();
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return;
        removeUser(user);
    }

}
