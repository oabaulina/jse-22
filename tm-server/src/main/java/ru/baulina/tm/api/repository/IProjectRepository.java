package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull final Long userId, @NotNull final Project project);

    void remove(@NotNull final Long userId, @NotNull final Project project);

    void clear(@NotNull final Long userId);

    @NotNull
    List<Project> findAll(@NotNull final Long userId);

    @Nullable
    Project findOneById(@NotNull final Long userId, @NotNull final Long id);

    @Nullable
    Project findOneByIndex(@NotNull final Long userId, @NotNull final Integer index);

    @Nullable
    Project findOneByName(@NotNull final Long userId, @NotNull final String name);

    @Nullable
    Project removeOneById(@NotNull final Long userId, @NotNull final Long id);

    @Nullable
    Project removeOneByIndex(@NotNull final Long userId, @NotNull final Integer index);

    @Nullable
    Project removeOneByName(@NotNull final Long userId, @NotNull final String name);

}
