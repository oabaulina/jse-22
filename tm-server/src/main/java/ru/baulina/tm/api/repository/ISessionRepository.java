package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void add(@NotNull final Session session);

    void remove(@NotNull final Session session);

    @NotNull
    List<Session> findByUserId(@NotNull final Long userId);

    @Nullable
    Session findById(@NotNull final Long id);

    void removeByUserId(@NotNull final Long userId);

    boolean contains(@NotNull final Long userId);

}
