package ru.baulina.tm.api.endpoint;

import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    List<User> getUserList(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    User findUserById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    User findUserByLogin(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    User removeUserById(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    void removeUserByLogin(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void lockUserLogin(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void unlockUserLogin(
            @WebParam(name = "session", partName = "session") Session session,
            @WebParam(name = "login", partName = "login") String login
    );

}
