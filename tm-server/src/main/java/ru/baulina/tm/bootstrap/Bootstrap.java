package ru.baulina.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.*;
import ru.baulina.tm.endpoint.*;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.repository.ProjectRepository;
import ru.baulina.tm.repository.SessionRepository;
import ru.baulina.tm.repository.TaskRepository;
import ru.baulina.tm.repository.UserRepository;
import ru.baulina.tm.service.*;

import javax.xml.ws.Endpoint;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IServiceLocator serviceLocator = this;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final SessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final SessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(userService, projectService, taskService);

    @NotNull
    private final AdminDumpEndpoint adminDampEndpoint = new AdminDumpEndpoint(serviceLocator);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(serviceLocator);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @SneakyThrows
    private void registryEndpoints(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServiceHost();
        final Integer port = propertyService.getServicePort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initProperty() throws Exception {
        propertyService.init();
    }

    private void initUsers()  {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initEndpoints() {
        registryEndpoints(adminUserEndpoint);
        registryEndpoints(adminDampEndpoint);
        registryEndpoints(projectEndpoint);
        registryEndpoints(sessionEndpoint);
        registryEndpoints(taskEndpoint);
        registryEndpoints(userEndpoint);
    }

    public void run(final String[] args) throws Exception {
        initProperty();
        initEndpoints();
        initUsers();
    }

}
