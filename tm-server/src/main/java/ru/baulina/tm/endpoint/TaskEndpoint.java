package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.api.endpoint.ITaskEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    public TaskEndpoint() {
    }

    public TaskEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public @NotNull List<Task> getTaskList() {
        return serviceLocator.getTaskService().getList();
    }

    @Override
    @WebMethod
    public void loadTasks(
            @WebParam(name = "tasks", partName = "tasks") List<Task> tasks
    ) {
        serviceLocator.getTaskService().load(tasks);
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getTaskService().create(userId, name);
    }

    @Override
    @WebMethod
    public void createTaskWithDescription(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getTaskService().create(userId, name, description);
    }

    @Override
    @WebMethod
    public void addTask(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "task", partName = "task") Task task
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getTaskService().add(userId, task);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "task", partName = "task") Task task
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getTaskService().remove(userId, task);
    }

    @Override
    @WebMethod
    public List<Task> findAllTasks(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().findAll(userId);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getTaskService().clear(userId);
    }

    @Override
    @WebMethod
    public Task findOneTaskById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().findOneById(userId, id);
    }

    @Override
    @WebMethod
    public Task findOneTaskByIndex(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().findOneByIndex(userId, index);
    }

    @Override
    @WebMethod
    public Task findOneTaskByName(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().findOneByName(userId, name);
    }

    @Override
    @WebMethod
    public Task removeOneTaskById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().removeOneById(userId, id);
    }

    @Override
    @WebMethod
    public Task removeOneTaskByIndex(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().removeOneByIndex(userId, index);
    }

    @Override
    @WebMethod
    public Task removeOneTaskByName(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().removeOneByName(userId, name);
    }

    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
    }

    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
    }

}
